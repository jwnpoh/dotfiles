call plug#begin('~/.vim/plugged')

" Declare the list of plugins.
Plug 'itchyny/lightline.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'justinmk/vim-sneak'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()
