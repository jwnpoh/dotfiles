source ~/.config/nvim/general/settings.vim
source ~/.config/nvim/vim-plug/plugins.vim
source ~/.config/nvim/keys/mappings.vim
source ~/.config/nvim/keys/which-key.vim
source ~/.config/nvim/vim-plug/goyo.vim
