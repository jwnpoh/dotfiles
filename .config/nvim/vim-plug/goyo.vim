autocmd! User GoyoEnter Limelight
autocmd! User GoyoEnter Pencil
autocmd! User GoyoLeave Limelight!


let g:goyo_height = 80
let g:goyo_width = 95
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_default_coefficient = 0.1
