" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    Plug 'itchyny/lightline.vim'
    Plug 'justinmk/vim-sneak'
    Plug 'liuchengxu/vim-which-key'
    Plug 'tpope/vim-commentary'
    Plug 'junegunn/goyo.vim'
    Plug 'junegunn/limelight.vim'
    Plug 'reedes/vim-pencil'
    Plug 'fatih/vim-go'
    Plug 'neoclide/coc.nvim'
    Plug 'tpope/vim-surround'

call plug#end()

"let g:lightline = {
"      \ 'colorscheme': 'nord'
"      \ }

if !has('gui_running')
  set t_Co=256
endif


let g:sneak#label = 1

let g:pencil#wrapModeDefault = 'soft'

let g:go_def_mode='gopls'
let g:go_info_mode='gopls'
